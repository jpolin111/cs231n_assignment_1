import numpy as np
from random import shuffle
from past.builtins import xrange

def softmax_loss_naive(W, X, y, reg):
  """
  Softmax loss function, naive implementation (with loops)

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  
  # Iterate over each example
  num_examples = X.shape[0]
  num_classes = W.shape[1]
  for example_idx in xrange(0, num_examples):
      x = X[example_idx,:]
      scores = x.dot(W)

      # --- Loss --- #

      # To improve numerical stability, subtract the largest score.
      scores -= np.max(scores)


      # Update loss for this example
      loss += -scores[y[example_idx]] + np.log(np.exp(scores).sum())

      # --- Derivative of Loss --- #

      # Binary vector with 1 where 
      correct_class_flags = np.zeros(num_classes)
      correct_class_flags[y[example_idx]] = 1.0

      # The coefficients for the x vector per class.
      coeffs = np.exp(scores) / np.exp(scores).sum() - correct_class_flags

      # Multiply coefficients by x and add to dW. Divide by N at end.
      dW += coeffs * np.tile(x,(num_classes, 1)).transpose()

  # Divide by number of examples.
  loss /= num_examples
  dW /= num_examples

  # Regularization.
  loss += reg * np.sum(W ** 2)
  dW += 2 * reg * W
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
  """
  Softmax loss function, vectorized version.

  Inputs and outputs are the same as softmax_loss_naive.
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  
  # --- Loss --- #

  # Scores for all examples in all classes.
  scores = X.dot(W)

  # Adjust for numerical error by subtracting the max score of each row from it.
  scores -= scores.max(axis=1).reshape((-1,1))

  # Contribution from correct class for each example.
  loss_per_example = -scores[[np.arange(y.size), y]]

  # Contribution of loss from wrong classes.
  loss_per_example += np.log(np.exp(scores).sum(axis=1))

  # Normalize by class and add regularization.
  loss = loss_per_example.mean() + reg * np.sum(W ** 2)

  # --- Derivative of Loss --- #

  # Matrix that's ones where j = y[i]. This times -x accounts for derivative
  # from the correct scores.
  correct_mask = np.zeros_like(scores)
  correct_mask[xrange(0,y.size),y] = 1

  # Scores normalized by sum of exponentiated scores for whole row.
  normalized_scores = np.exp(scores) / np.exp(scores).sum(axis=1).reshape((-1,1))

  # Combine these to get the coeffs for each example
  dW = np.dot((normalized_scores - correct_mask).T, X).T / X.shape[0]

  # Regularization
  dW += 2 * reg * W

  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW

