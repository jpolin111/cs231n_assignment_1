import numpy as np
from random import shuffle
from past.builtins import xrange

def svm_loss_naive(W, X, y, reg):
  """
  Structured SVM loss function, naive implementation (with loops).

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  dW = np.zeros(W.shape) # initialize the gradient as zero

  # compute the loss and the gradient
  num_classes = W.shape[1]
  num_train = X.shape[0]
  loss = 0.0
  for i in xrange(num_train):
    # print("Starting iteration %d of %d" % (i, num_train))
    scores = X[i].dot(W)
    correct_class_score = scores[y[i]]
    for j in xrange(num_classes):
      margin = scores[j] - correct_class_score + 1 # note delta = 1
      # Non-positive margin has no impact on loss or gradient
      if margin <= 0:
        continue
      # If this is the correct class, only update gradient. If we had a non-zero margin,
      # then the correct sample is in the direction that should minimize loss.
      if j == y[i]:
          # Count how many mis-labels we had by seeing how many margins are >0.
        num_pos_margins = (scores - correct_class_score + 1 > 0).sum()
        # The correct class score will inevitably be counted above since we'll have
        # 1 > 0. However, it should not count in the derivative since we don't use
        # j == y[i] for our loss.
        num_pos_margins -= 1
        dW[:, j] -= X[i] * num_pos_margins
      # If this is the incorrect class and margin > 0, update both loss and dW. Since this
      # sample is violating the margin, then this direction should add loss.
      else:
        loss += margin
        dW[:, j] += X[i]

  # Right now the loss is a sum over all training examples, but we want it
  # to be an average instead so we divide by num_train.
  loss /= num_train
  dW /= num_train

  # Add regularization to the loss.
  loss += reg * np.sum(W * W)
  dW += reg * 2 * W

  #############################################################################
  # TODO:                                                                     #
  # Compute the gradient of the loss function and store it dW.                #
  # Rather that first computing the loss and then computing the derivative,   #
  # it may be simpler to compute the derivative at the same time that the     #
  # loss is being computed. As a result you may need to modify some of the    #
  # code above to compute the gradient.                                       #
  #                                                                           #
  # Note from Joe: Not sure I did this right; off by ~5% = close?             #
  #                                                                           #
  #############################################################################


  return loss, dW


def svm_loss_vectorized(W, X, y, reg):
  """
  Structured SVM loss function, vectorized implementation.

  Inputs and outputs are the same as svm_loss_naive.
  """
  loss = 0.0

  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the structured SVM loss, storing the    #
  # result in loss.                                                           #
  #############################################################################
  # Mask for XW that leaves correct scores alone and makes wrong scores 0.
  correct_mask = np.zeros((X.shape[0], W.shape[1]))
  example_idx = xrange(X.shape[0])
  correct_mask[example_idx, y[example_idx]] = 1

  # Correct scores; sum to collapse to 1 dimension.
  correct_scores = (X.dot(W) * correct_mask).sum(1)

  # Margin matrix (need to make correct scores a column matrix to broadcast).
  M = X.dot(W) - np.reshape(correct_scores, (correct_scores.size, 1)) + 1
  positive_margin_mask = np.zeros(M.shape)
  positive_margin_mask[(M > 0)] = 1

  # Zero-out elements where j = yi
  positive_margin_mask[example_idx, y[example_idx]] = 0

  # Get rid of any negative entries.
  M *= positive_margin_mask

  # Loss is sum of positive margins where j != y[i]
  loss = M.sum(1).mean() + reg * np.sum(W * W)

  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################


  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the gradient for the structured SVM     #
  # loss, storing the result in dW.                                           #
  #                                                                           #
  # Hint: Instead of computing the gradient from scratch, it may be easier    #
  # to reuse some of the intermediate values that you used to compute the     #
  # loss.                                                                     #
  #############################################################################

  # Get the derivative from the incorrect scores. Take every x that contributed
  # to loss by being a mis-classification (ie margin was positive).
  incorrect = positive_margin_mask.transpose()
  incorrect_deriv = incorrect.dot(X)

  # Get the derivatives from correct scores. For each w, we need to
  correct = np.zeros(incorrect.shape)
  correct[y[example_idx], example_idx] = positive_margin_mask.sum(1)
  correct_deriv = correct.dot(X)

  # Combine to get dW.
  dW = np.transpose(incorrect_deriv - correct_deriv)

  # Normalize
  dW /= X.shape[0]

  # Don't forget regularization.
  dW += reg * 2 * W

  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################

  return loss, dW
